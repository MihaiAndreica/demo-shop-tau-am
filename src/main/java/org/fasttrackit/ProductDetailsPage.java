package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class ProductDetailsPage {
    private final SelenideElement addToCartButton = $(".fa-cart-plus");
    private final SelenideElement addToWishlistButton = $(".fa-heart.fa-3x");

    public void clickOnTheAddToCartIcon() {
        addToCartButton.click();
    }

    public void clickOnTheAddToWishlist() {
        addToWishlistButton.click();
    }
}
