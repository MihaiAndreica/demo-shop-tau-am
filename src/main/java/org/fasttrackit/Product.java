package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Product {

    private final SelenideElement productCard;
    private final SelenideElement productLink;
    private final SelenideElement addToCartButton;
    private final SelenideElement addToWishlistButton;
    private String name;
    private String price;
    private String cssSelector;

    public Product(String productId, String name, String price) {
        String selector = "[href='#/product/" + productId + "']"; // this also works.
        String productSelector = String.format("[href='#/product/%s']", productId);
        this.productLink = $(productSelector);
        this.productCard = this.productLink.parent().parent();
        this.addToCartButton = productCard.$(".fa-cart-plus");
        this.addToWishlistButton = productCard.$(".fa-heart");
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public void clickOnTheProductLink() {
        productLink.click();
    }

    public void addToCart() {
        System.out.println("Clicked on the " + addToCartButton + " on " + name);
        this.addToCartButton.scrollTo();
        this.addToCartButton.click();
    }
    public void addToWishlist() {
        this.addToWishlistButton.scrollTo();
        this.addToWishlistButton.click();
    }
    @Override
    public String toString() {
        return name;
    }

    public void scrollTo() {
        Selenide.executeJavaScript("arguments[0].scrollIntoView({behavior: 'smooth', block: 'center'})", $(cssSelector));
    }
}
