package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class CartPage extends Page {
    private final String CURRENCY = "$";
    private List<Product> productsInCart = new ArrayList<>();
    private final ElementsCollection distinctCartProducts = $$(".row a");
    private final SelenideElement cartTotalAmount = $(".amount-total .amount");
    private final ElementsCollection incrementButton = $$(".row .fa-plus-circle");
    private final ElementsCollection decrementButton = $$(".row .fa-minus-circle");
    private final ElementsCollection deleteButton = $$(".row .fa-trash");
    private final SelenideElement checkoutButton = $(".fa-angle-right");
    private int index;
//    private final SelenideElement productRow = $(".row");


    public int getNumberOfDistinctProducts() {
        return distinctCartProducts.size();
    }

    public int getTotalProductsInCart() {
        int totalProducts = 0;
        for (SelenideElement product : distinctCartProducts) {
            SelenideElement row = product.parent().parent();
            SelenideElement div = row.$("div");
            String numberOfProductsFromType = div.text();
            totalProducts += Integer.parseInt(numberOfProductsFromType);
        }
        return totalProducts;
    }

    public List<Product> getProductsInCart() {
        return productsInCart;
    }

    public double getTotalCartCostBasedOnProducts() {
        double totalCartCost = 0.0;
        for (SelenideElement product : distinctCartProducts) {
            SelenideElement row = product.parent().parent();
            String productPrice = row.$(".col-md-auto", 1).text().replace(CURRENCY, "");
            String numberOfProductsFromType = row.$("div").text().replace(CURRENCY, "");
            double pricePerProduct = Double.parseDouble(productPrice);
            double productsFromType = Double.parseDouble(numberOfProductsFromType);
            totalCartCost += (productsFromType * pricePerProduct);
        }
        NumberFormat format = new DecimalFormat("#0.00");
        return Double.parseDouble(format.format(totalCartCost));
    }

    public double getTotalCartAmount() {
        String totalWithoutCurrency = cartTotalAmount.text().replace(CURRENCY, "");
        return Double.parseDouble(totalWithoutCurrency);
    }

    public void incrementProductQuantity(int index) {
        incrementButton.get(index).click();
    }

    public void decrementProductQuantity(int productIndex) {
        decrementButton.get(index).click();
    }

    public void removeProductFromCart(int productIndex) {
        deleteButton.get(index).click();
    }
    public  void selectCheckoutButton() {
        checkoutButton.click();
    }
}
