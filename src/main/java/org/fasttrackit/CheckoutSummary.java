package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CheckoutSummary extends Page {
    private final SelenideElement completeYourOrderButton = $(".fa-angle-right");
    private final SelenideElement cancelButton = $(".fa-angle-left");

    public void selectCompleteYourOrder() {
        completeYourOrderButton.click();
    }
    public void cancelCheckout() {
        cancelButton.click();
    }
}