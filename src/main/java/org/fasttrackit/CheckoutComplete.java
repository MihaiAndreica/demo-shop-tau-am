package org.fasttrackit;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CheckoutComplete extends Page {
    private final SelenideElement completeText = $(".text-center");
    private final SelenideElement continueShopping = $(".fa-angle-right");
    public void validateCheckoutComplete() {
        completeText.exists();
        completeText.isDisplayed();
        completeText.shouldHave(Condition.exactText("Thank you for your order!"));
    }
    public void clickContinueShopping() {
        continueShopping.click();
    }

    public String getCheckoutCompleteText() {
        return completeText.text();
    }
}
