package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Selenide.$;

public class CheckoutInfo extends Page {
    private final SelenideElement firstNameField = $("#first-name");
    private final SelenideElement lastNameField = $("#last-name");
    private final SelenideElement addressField = $("#address");
    private final SelenideElement continueCheckoutButton = $(".fa-angle-right");
    private final SelenideElement cancelButton = $(".fa-angle-left");
    private SelenideElement checkoutErrorMsg = $(".error");

    public void fillInCheckoutForm(String firstName, String lastName, String address) {
        firstNameField.sendKeys(firstName);
        lastNameField.sendKeys(lastName);
        addressField.sendKeys(address);
        continueCheckoutButton.click();
    }
    public void cancelCheckout() {
        cancelButton.click();
    }
    public void validateErrorMessages() {
        firstNameField.shouldHave(attribute("value", ""));
        lastNameField.shouldHave(attribute("value", ""));
        addressField.shouldHave(attribute("value", ""));

        firstNameField.shouldHave(exactText("First Name is required"));
        lastNameField.shouldHave(exactText("Last Name is required"));
        addressField.shouldHave(exactText("Address is required"));
    }

    public boolean isDisplayed() {
        return this.firstNameField.isDisplayed() && this.lastNameField.isDisplayed() && this.addressField.isDisplayed();
    }
    public boolean isErrorMsgDisplayed() {
        return this.checkoutErrorMsg.exists() && this.checkoutErrorMsg.isDisplayed();
    }
    public String getCheckoutErrorMsg() {
        return checkoutErrorMsg.text();
    }
}
