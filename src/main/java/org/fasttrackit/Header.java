package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Header {
    private SelenideElement loginButton = $(".fa-sign-in-alt");
    private SelenideElement logoutButton = $(".fa-sign-out-alt");
    private SelenideElement greetingsElement = $(".navbar-text span span");
    private final SelenideElement cartIcon = $(".fa-shopping-cart");
    private final SelenideElement wishlistIcon = $(".fa-heart.fa-w-16");
    private SelenideElement cartBadge = $("[href='#/cart'] .shopping_cart_badge");
    private ElementsCollection cartBadges = $$("[href='#/cart'] .shopping_cart_badge");
    private SelenideElement wishlistBadge = $("[href='#/wishlist'] .shopping_cart_badge");
    private ElementsCollection wishlistBadges = $$("[href='#/wishlist'] .shopping_cart_badge");
    public Header() {

    }

    public String getNumberOfProductsInCart(){
        return this.cartBadge.text();
    }

    public boolean areAddedProductsInCart() {
        return cartBadges.size() > 0;
    }
    public String getNumberOfProductsInWishlist() {
        return this.wishlistBadge.text();
    }
    public boolean areAddedProductsInWishlist() {
        return wishlistBadges.size() > 0;
    }

    public void clickOnTheLoginButton() {
        loginButton.click();
    }

    public String getGreetingsMsg() {
        return greetingsElement.text();
    }
    public void clickOnTheCartIcon(){
        cartIcon.click();
    }
    public void clickOnTheWishlistIcon(){
        wishlistIcon.click();
    }

    public void clickOnTheLogoutButton() {
        logoutButton.click();
    }
}