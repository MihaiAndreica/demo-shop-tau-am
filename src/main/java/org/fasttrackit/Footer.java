package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Footer {
    private final SelenideElement resetPageButton = $(".fa-undo");

    public void resetPage(){
        this.resetPageButton.click();
    }
}
