package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class DemoShopPage extends Page {
    private SelenideElement searchField = $("#input-search");
    private SelenideElement searchButton = $(".btn-light");
    ElementsCollection productsOnPage = $$(".card");
    private Header header;
    private Footer footer;

    public DemoShopPage() {
        this.header = new Header();
        this.footer = new Footer();
    }

    public Header getHeader() {
        return header;
    }

    public Footer getFooter() {
        return footer;
    }
    public void clickOnSearchField() {
        searchField.click();
    }
    public void fillInSearchField(String searchKeyWord) {
        searchField.sendKeys(searchKeyWord);
    }
    public void clickOnSearchButton() {
        searchButton.click();
    }

    public ElementsCollection getProductsOnPage() {
        return productsOnPage;
    }
}