package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import static com.codeborne.selenide.Selenide.$$;

public class Wishlist extends Page {
    private final String CURRENCY = "$";
    private final ElementsCollection distinctWishlistProducts = $$(".row a");
    private final ElementsCollection addToCartFromWishlistButton = $$(".row .fa-cart-plus");
    private final ElementsCollection deleteProductFromWishlistButton = $$(".row .fa-heart-broken");
    private final ElementsCollection distinctProductsInWishlist = $$(".row a");
    private int index;

    public int getNumberOfDistinctProductsInWishlist() {
        return distinctWishlistProducts.size();}
    public void addToCartFromWishlist(int productIndex) {
        addToCartFromWishlistButton.get(index).click();
    }    public void removeProductFromWishlist(int productIndex) {
        deleteProductFromWishlistButton.get(index).click();
    }
}