package org.fasttrackit.products;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.DemoShopPage;
import org.fasttrackit.Footer;
import org.fasttrackit.LoginModal;
import org.fasttrackit.Product;
import org.fasttrackit.dataprovider.ProductsDataProvider;
import org.testng.annotations.*;

import static org.testng.Assert.assertEquals;
@Feature("Add all products to cart")
@Test(suiteName = "All products to cart testing, no user logged.")
public class ProductsCartVerificationUnloggedTest {
    DemoShopPage page;

    @BeforeMethod
    public void setup() {
        page = new DemoShopPage();
        page.openDemoShopApp();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.resetPage();
    }

    @AfterTest
    public void closeBrowser() {
        Selenide.closeWindow();
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "productsDataProvider", description = "No user logged in, add all products to cart.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("When no user is logged in, add one of all product types to the cart, the cart is updated correctly.")
    public void addAllProductTypeToCart(Object[] products) {
        for (Object product : products) {
            page.openDemoShopApp();
            Product prod = (Product) product;
            prod.addToCart();
        }

        String numberOfProductsInCart = page.getHeader().getNumberOfProductsInCart();
        String expectedNumberOfProductsInCart = String.valueOf(Integer.parseInt(page.getHeader().getNumberOfProductsInCart()));
        assertEquals(numberOfProductsInCart, expectedNumberOfProductsInCart, "Add one of each product to cart, the cart is updated correctly.");
    }
}
