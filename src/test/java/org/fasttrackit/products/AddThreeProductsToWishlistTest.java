package org.fasttrackit.products;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.DemoShopPage;
import org.fasttrackit.Footer;
import org.fasttrackit.Product;
import org.fasttrackit.ProductDetailsPage;
import org.fasttrackit.config.BaseTestConfig;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Test(suiteName = "Add to wishlist three different products.")
public class AddThreeProductsToWishlistTest extends BaseTestConfig {
    DemoShopPage page;
    @BeforeMethod
    public void setup() {
        this.page = new DemoShopPage();
        page.openDemoShopApp();
    }
    @AfterMethod
    public void cleanup() {
        System.out.println("Cleaning up after the test.");
        page.openDemoShopApp();
        Footer footer = new Footer();
        footer.resetPage();
    }

    @Test(description = "No user logged, from product details page add three different products to wishlist.")
    @Severity(SeverityLevel.NORMAL)
    @Description("When no user is logged, from product details page add three different products to wishlist, the wishlist is correctly updated.")
    public void add_to_wishlist_three_products_from_product_details_page() {
        Product metalChair = new Product("3", "Awesome Metal Chair", "15.99");
        metalChair.clickOnTheProductLink();
        ProductDetailsPage chairDetailsPage = new ProductDetailsPage();
        chairDetailsPage.clickOnTheAddToWishlist();

        page.openDemoShopApp();
        Product softShirt = new Product("5", "Awesome Soft Shirt", "29.99");
        softShirt.clickOnTheProductLink();
        ProductDetailsPage shirtDetailsPage = new ProductDetailsPage();
        shirtDetailsPage.clickOnTheAddToWishlist();

        page.openDemoShopApp();
        Product steelGloves = new Product("8","Licensed Steel Gloves","14.99");
        steelGloves.clickOnTheProductLink();
        ProductDetailsPage steelDetailsPage = new ProductDetailsPage();
        steelDetailsPage.clickOnTheAddToWishlist();

        String numberOfProductsInWishlist = page.getHeader().getNumberOfProductsInWishlist();
        assertEquals(numberOfProductsInWishlist, "3", "Adding 3 products to wishlist, so wishlist badge should display 3.");
    }

    @Test(description = "No user logged, from product list add three different products to wishlist.")
    @Severity(SeverityLevel.NORMAL)
    @Description("When no user is logged, from product list add three different products to wishlist, the wishlist is correctly updated.")
    public void add_to_wishlist_three_products_from_product_list() {
        Product metalChair = new Product("3", "Awesome Metal Chair", "15.99");
        metalChair.addToWishlist();

        Product softShirt = new Product("5", "Awesome Soft Shirt", "29.99");
        softShirt.addToWishlist();

        Product steelGloves = new Product("8","Licensed Steel Gloves","14.99");
        steelGloves.addToWishlist();

        String numberOfProductsInWishlist = page.getHeader().getNumberOfProductsInWishlist();
        assertEquals(numberOfProductsInWishlist, "3", "Adding 3 products to wishlist, so wishlist badge should display 3.");
    }
}