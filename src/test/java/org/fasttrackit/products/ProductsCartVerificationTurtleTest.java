package org.fasttrackit.products;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.DemoShopPage;
import org.fasttrackit.Footer;
import org.fasttrackit.LoginModal;
import org.fasttrackit.Product;
import org.fasttrackit.dataprovider.ProductsDataProvider;
import org.testng.annotations.*;

import static org.testng.Assert.assertEquals;
@Feature("Add all products to cart.")
@Test(suiteName = "All products to cart testing under 'turtle' user.")
public class ProductsCartVerificationTurtleTest {
    DemoShopPage page;

    @BeforeTest
    public void setup() {
        page = new DemoShopPage();
        page.openDemoShopApp();
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername("turtle");
        loginModal.fillInPassword("choochoo");
        loginModal.clickSubmitButton();
    }

    @AfterTest
    public void cleanup() {
        Footer footer = new Footer();
        footer.resetPage();
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "productsDataProvider", description = "'Turtle' logged in, add all products to cart.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("When 'turtle' user is logged in, add one of all product types to the cart, the cart is updated correctly.")
    public void addAllProductTypeToCart(Object[] products) {
        for (Object product : products) {
            page.openDemoShopApp();
            Product prod = (Product) product;
            prod.addToCart();
        }

        String numberOfProductsInCart = page.getHeader().getNumberOfProductsInCart();
        assertEquals(numberOfProductsInCart, "10", "'Turtle' logged in, add one of each product to cart, the cart is updated correctly to 10.");
    }
}