package org.fasttrackit.products;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.*;
import org.fasttrackit.config.BaseTestConfig;
import org.fasttrackit.dataprovider.User;
import org.fasttrackit.dataprovider.UserDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;

@Test(suiteName = "Add to cart one product.")
public class AddMetalMouseToCartTest extends BaseTestConfig {

    DemoShopPage page;

    @BeforeMethod
    public void setup() {
        this.page = new DemoShopPage();
        page.openDemoShopApp();

        Product metalMagicMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMagicMouse.clickOnTheProductLink();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.resetPage();
    }

    @Test(description = "No user logged, from product list add one metal mouse to cart.")
    @Severity(SeverityLevel.BLOCKER)
    @Description("When no user is logged, from product list add one metal mouse to cart, the cart is correctly updated.")
    public void add_to_cart_practical_metal_mouse_from_product_list_number_of_products_in_cart_is_one(){
        this.page = new DemoShopPage();
        page.openDemoShopApp();
        Product metalMagicMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMagicMouse.addToCart();


        String numberOfProductsInCart = page.getHeader().getNumberOfProductsInCart();
        assertEquals(numberOfProductsInCart, "1", "Adding one product to cart, the cart badge is 1.");
    }
    @Test(description = "No user logged, add one metal mouse to cart.")
    @Severity(SeverityLevel.BLOCKER)
    @Description("When no user is logged, from product details page add one metal mouse to cart, the cart is correctly updated.")
    public void add_practical_metal_mouse_product_in_cart_number_of_products_in_cart_is_one(){
        ProductDetailsPage detailsPage = new ProductDetailsPage();
        detailsPage.clickOnTheAddToCartIcon();

        String numberOfProductsInCart = page.getHeader().getNumberOfProductsInCart();
        assertEquals(numberOfProductsInCart, "1", "Adding one product to cart, the cart badge is 1.");
    }

    @Test(description = "No user logged, add two metal mouse to cart.")
    @Severity(SeverityLevel.BLOCKER)
    @Description("When no user is logged, from product details page add two metal mouse to cart, the cart is correctly updated.")
    public void add_two_practical_metal_mouse_products_in_cart_number_of_products_in_cart_is_two(){
        ProductDetailsPage detailsPage = new ProductDetailsPage();
        detailsPage.clickOnTheAddToCartIcon();
        detailsPage.clickOnTheAddToCartIcon();

        String numberOfProductsInCart = page.getHeader().getNumberOfProductsInCart();
        assertEquals(numberOfProductsInCart, "2", "Adding two products to cart, the cart badge is 2.");
    }

    @Test(description = "No user logged, from product list add two metal mouse to cart.")
    @Severity(SeverityLevel.BLOCKER)
    @Description("When no user is logged, from product list add two metal mouse to cart, the cart is correctly updated.")
    public void add_to_cart_two_practical_metal_mouse_from_product_list_number_of_products_in_cart_is_two(){
        this.page = new DemoShopPage();
        page.openDemoShopApp();
        Product metalMagicMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMagicMouse.addToCart();
        metalMagicMouse.addToCart();

        String numberOfProductsInCart = page.getHeader().getNumberOfProductsInCart();
        assertEquals(numberOfProductsInCart, "2", "Adding two product to cart, the cart badge is 2.");
    }

    @Test(description = "No user logged, add one metal mouse to cart and check cart cost.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("When no user is logged, from product details page add one metal mouse to cart, the cart cost is correctly displayed.")
    public void add_metal_mouse_total_cost_is_correctly_added() {
        ProductDetailsPage detailsPage = new ProductDetailsPage();
        detailsPage.clickOnTheAddToCartIcon();

        page.getHeader().clickOnTheCartIcon();
        CartPage cartPage = new CartPage();
        double totalCartCost = cartPage.getTotalCartCostBasedOnProducts();
        double totalCartAmount = cartPage.getTotalCartAmount();
        assertEquals(totalCartCost, 9.99, "The total products is 9.99");
        assertEquals(totalCartAmount, 9.99, "The cart total is 9.99");
    }

    @Test(description = "No user logged, add two metal mouse to cart and check cart cost.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("When no user is logged, from product details page add two metal mouse to cart, the cart cost is correctly displayed.")
    public void add_two_metal_mouse_total_cost_is_correctly_added() {
        ProductDetailsPage detailsPage = new ProductDetailsPage();
        detailsPage.clickOnTheAddToCartIcon();
        detailsPage.clickOnTheAddToCartIcon();

        page.getHeader().clickOnTheCartIcon();
        CartPage cartPage = new CartPage();
        double totalCartCost = cartPage.getTotalCartCostBasedOnProducts();
        double totalCartAmount = cartPage.getTotalCartAmount();
        assertEquals(totalCartCost, 19.98, "The total products is 19.98");
        assertEquals(totalCartAmount, 19.98, "The cart total is 19.98");
    }
}