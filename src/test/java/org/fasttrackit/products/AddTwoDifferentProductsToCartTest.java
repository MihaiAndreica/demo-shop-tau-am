package org.fasttrackit.products;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.CartPage;
import org.fasttrackit.DemoShopPage;
import org.fasttrackit.Footer;
import org.fasttrackit.Product;
import org.fasttrackit.config.BaseTestConfig;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Test(suiteName = "Add two different products from product list.")

public class AddTwoDifferentProductsToCartTest extends BaseTestConfig {
    DemoShopPage page;

    @BeforeTest
    public void setup() {
        page = new DemoShopPage();
        page.openDemoShopApp();
    }

    @BeforeMethod
    public void prerequisites() {
        Product metalMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMouse.addToCart();
        Product softPizza = new Product("9", "Gorgeous Soft Pizza", "19.99");
        softPizza.addToCart();
    }

    @AfterMethod
    public void cleanup() {
        System.out.println("Cleaning up after the test.");
        page.openDemoShopApp();
        Footer footer = new Footer();
        footer.resetPage();
    }

    @Test(description = "No user logged, add two different products to cart.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("When no user is logged, from add two different products to cart, the cart badge is correctly updated.")
    public void adding_metal_mouse_and_gorgeous_pizza_to_cart_two_products_are_in_cart() {
        String numberOfProductsInCart = page.getHeader().getNumberOfProductsInCart();
        assertEquals(numberOfProductsInCart, "2", "Adding 2 different products in cart.");
    }

    @Test(description = "No user logged, add two different products to cart.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("When no user is logged, from add two different products to cart, then go to cart to see is correctly updated.")
    public void adding_metal_mouse_and_gorgeous_pizza_to_cart_navigate_to_cart_page_two_products_are_in_cart() {
        page.getHeader().clickOnTheCartIcon();
        CartPage cartPage = new CartPage();
        int numberOfDistinctProducts = cartPage.getNumberOfDistinctProducts();
        int totalProductsInCart = cartPage.getTotalProductsInCart();

        assertEquals(numberOfDistinctProducts, 2, "Adding 2 different products to cart");
        assertEquals(totalProductsInCart, 2, "Total products in cart are 2");
    }

    @Test(description = "No user logged, add two different products to cart, the total cart cost is correctly calculated.")
    @Severity(SeverityLevel.BLOCKER)
    @Description("When adding 2 different products to the cart, the total cost is correctly calculated.")
    public void adding_metal_mouse_and_Gorgeous_pizza_products_to_cart_total_cost_is_correctly_added() {
        page.getHeader().clickOnTheCartIcon();
        CartPage cartPage = new CartPage();
        double totalCartCost = cartPage.getTotalCartCostBasedOnProducts();
        double totalCartAmount = cartPage.getTotalCartAmount();
        assertEquals(totalCartCost, 29.98, "The total products is 29.98");
        assertEquals(totalCartAmount, 29.98, "The cart total is 29.98");
    }
}
