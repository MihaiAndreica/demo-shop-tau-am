package org.fasttrackit.products;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.DemoShopPage;
import org.fasttrackit.Footer;
import org.fasttrackit.LoginModal;
import org.fasttrackit.Product;
import org.fasttrackit.dataprovider.ProductsDataProvider;
import org.testng.annotations.*;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;

@Feature("Add all products to cart.")
@Test(suiteName = "All products to cart testing under 'beetle' user.")
public class ProductsCartVerificationBeetleTest {
    DemoShopPage page;

    @BeforeMethod
    public void setup() {
        page = new DemoShopPage();
        page.openDemoShopApp();
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername("beetle");
        loginModal.fillInPassword("choochoo");
        loginModal.clickSubmitButton();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.resetPage();
    }

    @AfterTest
    public void closeBrowser() {
        Selenide.closeWindow();
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "productsDataProvider", description = "'Beetle' logged in, add to the cart, one by one, each of the products.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("When 'beetle' user is logged in, add to the cart, one by one, each of the products, and the cart is updated correctly.")
    public void addEachProductToCartIndependently(Product prod) {
        page.openDemoShopApp();
        prod.addToCart();

        String numberOfProductsInCart = page.getHeader().getNumberOfProductsInCart();
        assertEquals(numberOfProductsInCart, "1", "'Beetle' logged in, adding to cart, one by one, each of the products, the cart badge is 1.");
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "productsDataProvider", description = "'Beetle' logged in, add all products to cart.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("When 'beetle' user is logged in, add one of all product types to the cart, the cart is updated correctly.")
    public void addAllProductTypeToCart(Object[] products) {
        for (Object product : products) {
            page.openDemoShopApp();
            Product prod = (Product) product;
            prod.addToCart();
        }

        String numberOfProductsInCart = page.getHeader().getNumberOfProductsInCart();
        assertEquals(numberOfProductsInCart, 10, "'Beetle' logged in, add one of each product to cart, the cart is updated correctly to 10.");
    }
}