package org.fasttrackit.checkout;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.*;
import org.fasttrackit.config.BaseTestConfig;
import org.fasttrackit.dataprovider.CheckoutDataProvider;
import org.fasttrackit.dataprovider.User;
import org.fasttrackit.dataprovider.UserDataProvider;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.*;

@Test(suiteName = "Checkout testing.")
public class CheckoutTest extends BaseTestConfig {
    DemoShopPage page;

    @BeforeMethod
    public void setup() {
        this.page = new DemoShopPage();
        page.openDemoShopApp();
        Product metalMagicMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMagicMouse.addToCart();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.resetPage();
    }

    @Test(description = "No user logged, one metal mouse in cart, finish checkout.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("When no user is logged, one metal mouse already in cart, checkout finished.")
    public void NoUserLoggedMetalMouseInCartCompleteOrder() {
        page.openCartPage();
        CartPage cartPage = new CartPage();
        cartPage.selectCheckoutButton();

        CheckoutInfo checkoutInfo = new CheckoutInfo();
        checkoutInfo.fillInCheckoutForm("Alin", "Barbu", "Congo, strada Principala din capitala, nr 1");

        CheckoutSummary checkoutSummary = new CheckoutSummary();
        checkoutSummary.selectCompleteYourOrder();

        CheckoutComplete checkoutComplete = new CheckoutComplete();
        String checkoutCompleteText = checkoutComplete.getCheckoutCompleteText();
        assertEquals(checkoutCompleteText, "Thank you for your order!");
    }
    @Test(description = "No user logged, one metal mouse in cart and metal chair in wishlist, finish checkout.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("When no user is logged, one metal mouse already in cart and metal chair in wishlist, complete checkout, continue shopping, user still the same, wishlist still contains metal chair.")
    public void NoUserLoggedMetalMouseInCartAndMetalChairInWishlistCompleteOrderContinueShopping() {
        Product metalChair = new Product("3", "Awesome Metal Chair", "15.99");
        metalChair.clickOnTheProductLink();
        ProductDetailsPage chairDetailsPage = new ProductDetailsPage();
        chairDetailsPage.clickOnTheAddToWishlist();
        page.openCartPage();
        CartPage cartPage = new CartPage();
        cartPage.selectCheckoutButton();

        CheckoutInfo checkoutInfo = new CheckoutInfo();
        checkoutInfo.fillInCheckoutForm("Alin", "Barbu", "Congo, strada Principala din capitala, nr 1");

        CheckoutSummary checkoutSummary = new CheckoutSummary();
        checkoutSummary.selectCompleteYourOrder();

        CheckoutComplete checkoutComplete = new CheckoutComplete();
        String checkoutCompleteText = checkoutComplete.getCheckoutCompleteText();
        assertEquals(checkoutCompleteText, "Thank you for your order!");
        checkoutComplete.clickContinueShopping();

        page.openDemoShopApp();
        assertEquals(page.getHeader().getGreetingsMsg(), "Hello guest!", "After checkout, still no user is logged. Welcome message is: Hello guest!");
        assertEquals(page.getHeader().getNumberOfProductsInWishlist(),"1", "After checkout, the number of products in wishlist: 1!");
    }
    @Test(dataProviderClass = CheckoutDataProvider.class, dataProvider = "validCheckoutDataProvider", description = "No user logged, one metal mouse in cart and metal chair in wishlist, finish checkout using data provider info.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("When no user is logged, one metal mouse already in cart and metal chair in wishlist, complete checkout using data provider billing info, continue shopping, user still the same, wishlist still contains metal chair.")
    public void NoUserLoggedMetalMouseInCartAndMetalChairInWishlistCompleteOrderWithValidCheckoutDataProviderContinueShopping(String firstName, String lastName, String address) {
        Product metalChair = new Product("3", "Awesome Metal Chair", "15.99");
        metalChair.clickOnTheProductLink();
        ProductDetailsPage chairDetailsPage = new ProductDetailsPage();
        chairDetailsPage.clickOnTheAddToWishlist();

        page.openCartPage();
        CartPage cartPage = new CartPage();
        cartPage.selectCheckoutButton();

        CheckoutInfo checkoutInfo = new CheckoutInfo();
        checkoutInfo.fillInCheckoutForm(firstName, lastName, address);

        CheckoutSummary checkoutSummary = new CheckoutSummary();
        checkoutSummary.selectCompleteYourOrder();

        CheckoutComplete checkoutComplete = new CheckoutComplete();
        String checkoutCompleteText = checkoutComplete.getCheckoutCompleteText();
        assertEquals(checkoutCompleteText, "Thank you for your order!");
        checkoutComplete.clickContinueShopping();

        page.openDemoShopApp();
        assertEquals(page.getHeader().getGreetingsMsg(), "Hello guest!", "After checkout, still no user is logged. Welcome message is: Hello guest!");
        assertEquals(page.getHeader().getNumberOfProductsInWishlist(), "1", "After checkout, the number of products in wishlist: 1!");
    }

    @Test(dataProviderClass = CheckoutDataProvider.class, dataProvider = "invalidCheckoutDataProvider", description = "No user logged, one metal mouse in cart and metal chair in wishlist, finish checkout using data provider invalid info.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("When no user is logged, one metal mouse already in cart and metal chair in wishlist, complete checkout using data provider billing invalid info, continue shopping, user still the same, wishlist still contains metal chair.")
    public void NoUserLoggedMetalMouseInCartAndMetalChairInWishlistCompleteOrderWithInvalidCheckoutDataContinueShopping(String firstName, String lastName, String address) {
        Product metalChair = new Product("3", "Awesome Metal Chair", "15.99");
        metalChair.clickOnTheProductLink();
        ProductDetailsPage chairDetailsPage = new ProductDetailsPage();
        chairDetailsPage.clickOnTheAddToWishlist();

        page.openCartPage();
        CartPage cartPage = new CartPage();
        cartPage.selectCheckoutButton();

        CheckoutInfo checkoutInfo = new CheckoutInfo();
        checkoutInfo.fillInCheckoutForm(firstName, lastName, address);

        assertTrue(checkoutInfo.isDisplayed(),"Checkout Info is still open.");
        assertTrue(checkoutInfo.isErrorMsgDisplayed(),"Error message is displayed");
    }

    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider", description = "Valid user logged in, one metal mouse in cart and metal chair in wishlist, finish checkout using data provider info.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("When valid user is logged in, one metal mouse already in cart and metal chair in wishlist, complete checkout using data provider billing info, continue shopping, user still the same, wishlist still contains metal chair.")
    public void validUserLoggedInMetalMouseInCartAndMetalChairInWishlistCompleteOrderAndContinueShopping(User user) {
        Product metalChair = new Product("3", "Awesome Metal Chair", "15.99");
        metalChair.clickOnTheProductLink();
        ProductDetailsPage chairDetailsPage = new ProductDetailsPage();
        chairDetailsPage.clickOnTheAddToWishlist();

        page.openCartPage();
        CartPage cartPage = new CartPage();
        cartPage.selectCheckoutButton();

        CheckoutInfo checkoutInfo = new CheckoutInfo();
        checkoutInfo.fillInCheckoutForm("Alex", "Strauss", "Bolentin Vale, str. Principala, nr. 759");

        CheckoutSummary checkoutSummary = new CheckoutSummary();
        checkoutSummary.selectCompleteYourOrder();

        CheckoutComplete checkoutComplete = new CheckoutComplete();
        String checkoutCompleteText = checkoutComplete.getCheckoutCompleteText();
        assertEquals(checkoutCompleteText, "Thank you for your order!");
        checkoutComplete.clickContinueShopping();

        page.openDemoShopApp();
        assertEquals(page.getHeader().getGreetingsMsg(), "Hello guest!", "After checkout, same user is logged in. Welcome message is: Hi");
        assertEquals(page.getHeader().getNumberOfProductsInWishlist(), "1", "After checkout, the number of products in wishlist: 1!");
    }
}
