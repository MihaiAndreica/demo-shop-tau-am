package org.fasttrackit.dataprovider;

import org.testng.annotations.DataProvider;

public class UserDataProvider {
    @DataProvider(name = "validUserDataProvider")
    public static Object[][] feedUserDataProvider() {
        User beetleUser = new User("beetle", "choochoo");
        User dinoUser = new User("dino", "choochoo");
        User turtleUser = new User("turtle", "choochoo");
        return new Object[][] {
                {beetleUser},
                {dinoUser},
                {turtleUser}
        };
    }
    @DataProvider(name = "invalidUserDataProvider")
    public static Object[][] feedInvalidUserDataProvider() {
        User lockedUser = new User("locked", "choochoo");
        lockedUser.setErrorMsg("The user has been locked out.");
        User invalidUser = new User("test", "choochoo");
        invalidUser.setErrorMsg("Incorrect username or password!");
        User invalidPass = new User("dino", "test");
        invalidPass.setErrorMsg("Incorrect username or password!");
        return new Object[][] {
                {lockedUser},
                {invalidUser},
                {invalidPass}
        };
    }
}
