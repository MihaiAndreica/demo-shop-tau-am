package org.fasttrackit.dataprovider;

import org.testng.annotations.DataProvider;

public class CheckoutDataProvider {
    @DataProvider(name = "validCheckoutDataProvider")
    public static Object[][] feedValidCheckoutDataProvider() {
        return new Object[][]{
                {"John", "Doe", "123 Main St"},
                {"Ion", "Popescu", "Dorohoi Vale, Principala 007"},
                {"Robert", "Smith", "789 Elm St"}
        };
    }

    @DataProvider(name = "invalidCheckoutDataProvider")
    public static Object[][] getInvalidCheckoutData() {
        return new Object[][]{
                {"", "Doe", "123 Main St"},
                {"John", "", "123 Main St"},
                {"John", "Doe", ""}
        };
    }
}
