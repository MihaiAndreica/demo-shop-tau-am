package org.fasttrackit.config;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class BaseTestConfig {
    public BaseTestConfig() {
        Configuration.browser = "Chrome";
        Configuration.headless = false;
    }
    @BeforeClass
    static void setupAllureReports() {
//        SelenideLogger.addListener("AllureSelenide", new AllureSelenide());

        // or for fine-tuning:
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide()
                .screenshots(true)
                .savePageSource(true)
        );
    }
    @AfterClass
    public void closeBrowser() {
        Selenide.closeWindow();
    }
}
