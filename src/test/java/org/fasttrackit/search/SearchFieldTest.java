package org.fasttrackit.search;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.DemoShopPage;
import org.fasttrackit.Footer;
import org.fasttrackit.LoginModal;
import org.fasttrackit.Product;
import org.fasttrackit.config.BaseTestConfig;
import org.fasttrackit.dataprovider.User;
import org.fasttrackit.dataprovider.UserDataProvider;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

@Test(suiteName = "Search field testing.")
public class SearchFieldTest extends BaseTestConfig {
    DemoShopPage page = new DemoShopPage();

    @BeforeMethod
    public void setup() {
        page.openDemoShopApp();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.resetPage();
    }

    @Test(description = "No user logged, search products by a specific word.")
    @Severity(SeverityLevel.MINOR)
    @Description("When no user is logged, search products by a specific word.")
    public void NoUserLoggedSearchAwesomeProducts() {
        page.clickOnSearchField();
        page.fillInSearchField("awesome");
        page.clickOnSearchButton();
        ElementsCollection productsFound = page.getProductsOnPage();
        for (SelenideElement product : productsFound) {
            Assert.assertTrue(product.text().toLowerCase().contains("awesome".toLowerCase()), "All products on page contain in title the keyword: 'awesome'.");
        }
    }

    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider", description = "Logged in user, search products by a specific word.")
    @Severity(SeverityLevel.MINOR)
    @Description("When user is logged in, search products by a specific word.")
    public void UserLoggedInSearchAwesomeProducts(User user) {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();

        page.clickOnSearchField();
        page.fillInSearchField("awesome");
        page.clickOnSearchButton();
        ElementsCollection productsFound = page.getProductsOnPage();
        for (SelenideElement product : productsFound) {
            Assert.assertTrue(product.text().toLowerCase().contains("awesome".toLowerCase()), "All products on page contain in title the keyword: 'awesome'.");
        }
    }
}
