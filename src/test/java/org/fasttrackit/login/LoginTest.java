package org.fasttrackit.login;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.DemoShopPage;
import org.fasttrackit.LoginModal;
import org.fasttrackit.config.BaseTestConfig;
import org.fasttrackit.dataprovider.User;
import org.fasttrackit.dataprovider.UserDataProvider;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Test(suiteName = "Login feature.")
public class LoginTest extends BaseTestConfig {
    DemoShopPage page;

    @BeforeMethod
    public void setup() {
        this.page = new DemoShopPage();
        page.openDemoShopApp();
    }
    @AfterMethod
    public void reset() {
        Selenide.refresh();
        page.getFooter().resetPage();
    }

    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider", description = "Login with valid users.")
    @Severity(SeverityLevel.NORMAL)
    @Description("Login with valid user credentials.")
    public void userCanLoginOnDemoShopPage(User user) {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();
        assertEquals(page.getHeader().getGreetingsMsg(), user.getExpectedGreetingsMsg(),"Greetings message is: " + user.getExpectedGreetingsMsg());
    }

    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "invalidUserDataProvider", description = "Login with invalid users.")
    @Severity(SeverityLevel.NORMAL)
    @Description("Login with locked user or invalid user credentials.")
    public void userCanNotLoginOnDemoShopPageWithInvalidUser(User user) {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();

        assertTrue(loginModal.isDisplayed(),"Login modal is still open.");
        assertTrue(loginModal.isErrorMsgDisplayed(),"Login error message is displayed");
        assertEquals(loginModal.getErrorMsg(), user.getErrorMsg(),"An error message is displayed: locked user/wrong username/wrong password.");
        assertEquals(page.getHeader().getGreetingsMsg(), user.getDefaultGreetingsMsg(), "Greetings message is: Hello guest!");
    }
}