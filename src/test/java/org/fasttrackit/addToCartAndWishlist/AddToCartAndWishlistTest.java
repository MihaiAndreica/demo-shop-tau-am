package org.fasttrackit.addToCartAndWishlist;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.*;
import org.fasttrackit.config.BaseTestConfig;
import org.fasttrackit.dataprovider.User;
import org.fasttrackit.dataprovider.UserDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

@Feature("Add to cart and wishlist.")
@Test(suiteName = "Add to cart and wishlist.")
public class AddToCartAndWishlistTest extends BaseTestConfig {
    DemoShopPage page;

    @BeforeMethod
    public void setup() {
        this.page = new DemoShopPage();
        page.openDemoShopApp();
    }

    @AfterMethod
    public void cleanup() {
        page.openDemoShopApp();
        Footer footer = new Footer();
        footer.resetPage();
    }

    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider", description = "Add 3 products to cart and 3 to wishlist, then login.")
    @Severity(SeverityLevel.NORMAL)
    @Description("When adding 3 products to cart and 3 to wishlist, then login, the account is updated correctly.")
    public void add_3_products_to_cart_and_3_to_wishlist_then_login(User user) {
        Product metalMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMouse.addToCart();
        Product graniteChips = new Product("1", "Awesome Granite Chips", "15.99");
        graniteChips.addToCart();
        Product softPizza = new Product("9", "Gorgeous Soft Pizza", "19.99");
        softPizza.addToCart();

        Product metalChair = new Product("3", "Awesome Metal Chair", "15.99");
        metalChair.addToWishlist();
        Product softShirt = new Product("5", "Awesome Soft Shirt", "29.99");
        softShirt.addToWishlist();
        Product steelGloves = new Product("8", "Licensed Steel Gloves", "14.99");
        steelGloves.addToWishlist();

        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();

        String numberOfProductsInWishlist = page.getHeader().getNumberOfProductsInWishlist();
        assertEquals(numberOfProductsInWishlist, "3", "Adding 3 products to wishlist, then login, so wishlist badge should display 3.");

        String numberOfProductsInCart = page.getHeader().getNumberOfProductsInCart();
        assertEquals(numberOfProductsInCart, "3", "Adding 3 different products to cart, then login, so cart badge should display 3.");
    }

    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider", description = "Logged in, add 1 products to cart and 1 to wishlist, from wishlist add product to cart and then delete it from wishlist.")
    @Severity(SeverityLevel.NORMAL)
    @Description("When logged in, add 1 products to cart and 1 to wishlist, from wishlist add product to cart and clear wishlist, cart is updated correctly and wishlist is empty.")
    public void add_1_products_to_cart_and_1_to_wishlist_move_product_from_wishlist_to_cart_and_empty_wishlist(User user) {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();

        Product metalMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMouse.clickOnTheProductLink();
        ProductDetailsPage metalMouseDetailsPage = new ProductDetailsPage();
        metalMouseDetailsPage.clickOnTheAddToCartIcon();

        page.openDemoShopApp();
        Product metalChair = new Product("3", "Awesome Metal Chair", "15.99");
        metalChair.clickOnTheProductLink();
        ProductDetailsPage metalChairDetailsPage = new ProductDetailsPage();
        metalChairDetailsPage.clickOnTheAddToWishlist();

        Wishlist wishlist = new Wishlist();
        wishlist.addToCartFromWishlist(0);
        wishlist.removeProductFromWishlist(0);

        boolean areThereProductsInWishlist = page.getHeader().areAddedProductsInWishlist();
        assertFalse(areThereProductsInWishlist, "Cart badge is not displayed when no products are added in cart.");

        String numberOfProductsInCart = page.getHeader().getNumberOfProductsInCart();
        assertEquals(numberOfProductsInCart, "2", "Adding 2 different products to cart, so cart badge should display 2.");
    }
}
