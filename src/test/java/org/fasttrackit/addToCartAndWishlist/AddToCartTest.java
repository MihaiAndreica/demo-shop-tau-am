package org.fasttrackit.addToCartAndWishlist;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.*;
import org.fasttrackit.config.BaseTestConfig;
import org.fasttrackit.dataprovider.User;
import org.fasttrackit.dataprovider.UserDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("Add to cart.")
@Test(suiteName = "Add to cart.")
public class AddToCartTest extends BaseTestConfig {
    DemoShopPage page;

    @BeforeMethod
    public void setup() {
        this.page = new DemoShopPage();
        page.openDemoShopApp();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.resetPage();
    }

    @Test(description = "No user logged in, add product to cart from product list.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("When no user is logged in, add to cart a product from product list, the cart is updated correctly.")
    public void noUserLoggedAddProductToCartFromProductList() {
        Product metalMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMouse.addToCart();

        boolean areProductsAdded = page.getHeader().areAddedProductsInCart();
        assertTrue(areProductsAdded, "Cart badge is displayed when products are added to cart.");
        String numberOfProductsInCart = page.getHeader().getNumberOfProductsInCart();
        assertEquals(numberOfProductsInCart, "1", "No user logged, add 1 product to cart.");
    }

    @Test(description = "No user logged in, add product to cart from product details page.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("When no user is logged in, add to cart a product from product details page, the cart is updated correctly.")
    public void noUserLoggedAddProductToCartFromProductDetailsPage() {
        Product metalMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMouse.clickOnTheProductLink();
        ProductDetailsPage metalMouseDetailsPage = new ProductDetailsPage();
        metalMouseDetailsPage.clickOnTheAddToCartIcon();

        boolean areProductsAdded = page.getHeader().areAddedProductsInCart();
        assertTrue(areProductsAdded, "Cart badge is displayed when products are added to cart.");
        String numberOfProductsInCart = page.getHeader().getNumberOfProductsInCart();
        assertEquals(numberOfProductsInCart, "1", "No user logged, add 1 product to cart.");
    }

    @Test(description = "No user logged in, add twice 2 different products to cart from product details page.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("When no user is logged in, add twice 2 different products from product details page, then go to cart to see products are correctly added.")
    public void noUserLoggedAddTwiceTwoProductsToCartFromProductDetailsPage() {
        Product metalMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMouse.clickOnTheProductLink();
        ProductDetailsPage metalMouseDetailsPage = new ProductDetailsPage();
        metalMouseDetailsPage.clickOnTheAddToCartIcon();
        metalMouseDetailsPage.clickOnTheAddToCartIcon();

        page.openDemoShopApp();
        Product awesomeMetalChair = new Product("3", "Awesome Metal Chair", "15.99");
        awesomeMetalChair.clickOnTheProductLink();
        ProductDetailsPage metalChairDetailsPage = new ProductDetailsPage();
        metalChairDetailsPage.clickOnTheAddToCartIcon();
        metalChairDetailsPage.clickOnTheAddToCartIcon();

        page.getHeader().clickOnTheCartIcon();
        CartPage cartPage = new CartPage();
        int numberOfDistinctProducts = cartPage.getNumberOfDistinctProducts();
        int totalProductsInCart = cartPage.getTotalProductsInCart();
        assertEquals(numberOfDistinctProducts, 2, "Adding 2 different products to cart");
        assertEquals(totalProductsInCart, 4, "Total products in cart is 4.");
    }

    @Test(description = "No user logged in, add twice 2 different products to cart from product details page.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("When no user is logged in, add twice 2 different products from product details page, the total cost is correctly calculated.")
    public void noUserLoggedAddTwiceTwoProductsToCartFromProductDetailsPageTotalCostCorrect() {
        Product metalMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMouse.clickOnTheProductLink();
        ProductDetailsPage metalMouseDetailsPage = new ProductDetailsPage();
        metalMouseDetailsPage.clickOnTheAddToCartIcon();
        metalMouseDetailsPage.clickOnTheAddToCartIcon();

        page.openDemoShopApp();
        Product awesomeMetalChair = new Product("3", "Awesome Metal Chair", "15.99");
        awesomeMetalChair.clickOnTheProductLink();
        ProductDetailsPage metalChairDetailsPage = new ProductDetailsPage();
        metalChairDetailsPage.clickOnTheAddToCartIcon();
        metalChairDetailsPage.clickOnTheAddToCartIcon();

        page.getHeader().clickOnTheCartIcon();
        CartPage cartPage = new CartPage();
        double totalCartCost = cartPage.getTotalCartCostBasedOnProducts();
        double totalCartAmount = cartPage.getTotalCartAmount();
        assertEquals(totalCartCost, 51.96, "Adding 2 metal mouse and 2 metal chair to cart, the products total cost is $51.96.");
        assertEquals(totalCartAmount, 51.96, "Adding 2 metal mouse and 2 metal chair to cart, the total cart amount cost is $51.96.");
    }

    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider", description = "Logged in user adds product from product list.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("When a logged in user adds to cart a product from product list, the cart is updated correctly.")
    public void loggedUserAddProductToCartFromProductList(User user) {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();

        Product metalMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMouse.addToCart();

        boolean areProductsAdded = page.getHeader().areAddedProductsInCart();
        assertTrue(areProductsAdded, "Cart badge is displayed when products are added to cart.");
        String numberOfProductsInCart = page.getHeader().getNumberOfProductsInCart();
        assertEquals(numberOfProductsInCart, "1", "Logged in user adds 1 product to cart.");
    }

    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider", description = "Logged in user adds product from product details page.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("When a logged in user adds to cart a product from product details page, the cart is updated correctly.")
    public void loggedUserAddProductToCartFromProductDetailsPage(User user) {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();

        Product metalMagicMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMagicMouse.clickOnTheProductLink();
        ProductDetailsPage detailsPage = new ProductDetailsPage();
        detailsPage.clickOnTheAddToCartIcon();

        boolean areProductsAdded = page.getHeader().areAddedProductsInCart();
        assertTrue(areProductsAdded, "Cart badge is displayed when products are added to cart.");
        String numberOfProductsInCart = page.getHeader().getNumberOfProductsInCart();
        assertEquals(numberOfProductsInCart, "1", "Logged in user adds 1 product to cart.");
    }

    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider", description = "User logged in, add two metal mouse to cart and check cart cost.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("When user is logged, from product details page add two metal mouse to cart, the cart cost is correctly displayed.")
    public void user_logged_in_add_two_metal_mouse_total_cost_is_correctly_added(User user) {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();

        Product metalMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMouse.clickOnTheProductLink();
        ProductDetailsPage metalMouseDetailsPage = new ProductDetailsPage();
        metalMouseDetailsPage.clickOnTheAddToCartIcon();
        metalMouseDetailsPage.clickOnTheAddToCartIcon();

        page.getHeader().clickOnTheCartIcon();
        CartPage cartPage = new CartPage();
        double totalCartCost = cartPage.getTotalCartCostBasedOnProducts();
        double totalCartAmount = cartPage.getTotalCartAmount();
        assertEquals(totalCartCost, 19.98, "The total products is 19.98");
        assertEquals(totalCartAmount, 19.98, "The cart total is 19.98");
    }

    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider", description = "User logged in, add twice 2 different products to cart from product details page.")
    @Severity(SeverityLevel.BLOCKER)
    @Description("When user is logged in, add twice 2 different products from product details page, the total cost is correctly calculated.")
    public void userLoggedAddTwiceTwoProductsToCartFromProductDetailsPageTotalCostCorrect(User user) {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();
        Product metalMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMouse.clickOnTheProductLink();
        ProductDetailsPage metalMouseDetailsPage = new ProductDetailsPage();
        metalMouseDetailsPage.clickOnTheAddToCartIcon();
        metalMouseDetailsPage.clickOnTheAddToCartIcon();

        page.openDemoShopApp();
        Product awesomeMetalChair = new Product("3", "Awesome Metal Chair", "15.99");
        awesomeMetalChair.clickOnTheProductLink();
        ProductDetailsPage metalChairDetailsPage = new ProductDetailsPage();
        metalChairDetailsPage.clickOnTheAddToCartIcon();
        metalChairDetailsPage.clickOnTheAddToCartIcon();

        page.getHeader().clickOnTheCartIcon();
        CartPage cartPage = new CartPage();
        double totalCartCost = cartPage.getTotalCartCostBasedOnProducts();
        double totalCartAmount = cartPage.getTotalCartAmount();
        assertEquals(totalCartCost, 51.96, "Adding 2 metal mouse and 2 metal chair to cart, the products total cost is $51.96.");
        assertEquals(totalCartAmount, 51.96, "Adding 2 metal mouse and 2 metal chair to cart, the total cart amount cost is $51.96.");
    }
}

