package org.fasttrackit.addToCartAndWishlist;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.*;
import org.fasttrackit.config.BaseTestConfig;
import org.fasttrackit.dataprovider.User;
import org.fasttrackit.dataprovider.UserDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.sleep;
import static org.testng.Assert.*;

@Feature("Increment, decrement, product delete from cart page.")
@Test(suiteName = "Increment, decrement, product delete from cart page.")
public class IncrementDecrementAndDeleteFromCartPageTest extends BaseTestConfig {

    DemoShopPage page;

    @BeforeMethod
    public void setup() {
        this.page = new DemoShopPage();
        page.openDemoShopApp();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.resetPage();
    }

    @Test(description = "No user logged, from product list add one metal mouse to cart and increment it from cart page.")
    @Severity(SeverityLevel.NORMAL)
    @Description("When no user is logged, from product list add one metal mouse to cart, then go to cart page and increment product, the cart is correctly updated to 2 products.")
    public void add_one_product_to_cart_and_increment_it() {
        Product metalMagicMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMagicMouse.addToCart();

        CartPage cartPage = new CartPage();
        cartPage.openCartPage();
        cartPage.incrementProductQuantity(0);

        String numberOfProductsInCart = page.getHeader().getNumberOfProductsInCart();
        assertEquals(numberOfProductsInCart, "2", "Adding one product to cart and incrementing it after, the cart badge is 2.");
    }
    @Test(description = "No user logged, from product list add one metal mouse to cart, increment it twice and decrement it once from cart page.")
    @Severity(SeverityLevel.NORMAL)
    @Description("When no user is logged, from product list add one metal mouse to cart, then go to cart page and increment product twice and decrement it once, the cart is correctly updated to 2 products.")
    public void add_one_product_to_cart_and_increment_it_twice_and_decrement_once() {
        Product metalMagicMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMagicMouse.addToCart();

        CartPage cartPage = new CartPage();
        cartPage.openCartPage();
        cartPage.incrementProductQuantity(0);
        cartPage.incrementProductQuantity(0);
        cartPage.decrementProductQuantity(0);

        String numberOfProductsInCart = page.getHeader().getNumberOfProductsInCart();
        assertEquals(numberOfProductsInCart, "2", "Adding one product to cart and incrementing it twice and decrementing once, the cart badge is 2.");
    }
    @Test(description = "No user logged, from product list add one metal mouse to cart, increment it once and decrement it twice from cart page.")
    @Severity(SeverityLevel.NORMAL)
    @Description("When no user is logged, from product list add one metal mouse to cart, then go to cart page and increment product once and decrement it twice, the cart is empty.")
    public void add_one_product_to_cart_and_increment_it_once_and_decrement_twice() {
        Product metalMagicMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMagicMouse.addToCart();

        CartPage cartPage = new CartPage();
        cartPage.openCartPage();
        cartPage.incrementProductQuantity(0);
        cartPage.decrementProductQuantity(0);
        cartPage.decrementProductQuantity(0);

        boolean areAddedProductsInCart = page.getHeader().areAddedProductsInCart();
        assertFalse(areAddedProductsInCart, "Cart badge is not displayed when no products are added in cart.");
    }
    @Test(description = "No user logged, from product list add one metal mouse to cart, increment it twice and remove product from cart page.")
    @Severity(SeverityLevel.NORMAL)
    @Description("When no user is logged, from product list add one metal mouse to cart, then go to cart page and increment product twice and remove product, the cart is empty.")
    public void add_one_product_to_cart_and_increment_it_twice_and_delete_product_from_cart() {
        Product metalMagicMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMagicMouse.addToCart();

        CartPage cartPage = new CartPage();
        cartPage.openCartPage();
        cartPage.incrementProductQuantity(0);
        cartPage.incrementProductQuantity(0);
        cartPage.removeProductFromCart(0);

        boolean areAddedProductsInCart = page.getHeader().areAddedProductsInCart();
        assertFalse(areAddedProductsInCart, "Cart badge is not displayed when no products are added in cart.");
    }

    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider", description = "Logged user, from product details page add one metal mouse to cart and increment it from cart page.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("When valid user is logged, from product details page add one metal mouse to cart, then go to cart page and increment product three times, the cart is correctly updated to 4 products.")
    public void logged_user_add_to_cart_one_metal_mouse_then_increment_it_three_times(User user) {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();

        Product metalMagicMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMagicMouse.clickOnTheProductLink();
        ProductDetailsPage metalMouseDetailsPage = new ProductDetailsPage();
        metalMouseDetailsPage.clickOnTheAddToCartIcon();

        CartPage cartPage = new CartPage();
        cartPage.openCartPage();
        cartPage.incrementProductQuantity(0);
        cartPage.incrementProductQuantity(0);
        cartPage.incrementProductQuantity(0);

        String numberOfProductsInCart = page.getHeader().getNumberOfProductsInCart();
        assertEquals(numberOfProductsInCart, "4", "Adding one product to cart and incrementing it after, the cart badge is 4.");
    }

    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider", description = "Logged user, from product details page add one metal mouse to cart, increment and decrement it from cart page.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("When valid user is logged, from product details page add one metal mouse to cart, then go to cart page and increment product three times, then decrement it twice, the cart is correctly updated to 2 products.")
    public void logged_user_add_to_cart_one_metal_mouse_then_increment_it_three_times_and_decrement_twice(User user) {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();

        Product metalMagicMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMagicMouse.clickOnTheProductLink();
        ProductDetailsPage metalMouseDetailsPage = new ProductDetailsPage();
        metalMouseDetailsPage.clickOnTheAddToCartIcon();

        CartPage cartPage = new CartPage();
        cartPage.openCartPage();
        cartPage.incrementProductQuantity(0);
        cartPage.incrementProductQuantity(0);
        cartPage.incrementProductQuantity(0);
        cartPage.decrementProductQuantity(0);
        cartPage.decrementProductQuantity(0);

        String numberOfProductsInCart = page.getHeader().getNumberOfProductsInCart();
        assertEquals(numberOfProductsInCart, "2", "Adding one product to cart and incrementing it after, the cart badge is 2.");
    }
    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider", description = "Logged user, from product details page add one metal mouse to cart, increment once and decrement it twice from cart page.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("When valid user is logged, from product details page add one metal mouse to cart, then go to cart page and increment product once, then decrement it twice, the cart is empty.")
    public void logged_user_add_to_cart_one_metal_mouse_then_increment_it_once_and_decrement_twice(User user) {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();

        Product metalMagicMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMagicMouse.clickOnTheProductLink();
        ProductDetailsPage metalMouseDetailsPage = new ProductDetailsPage();
        metalMouseDetailsPage.clickOnTheAddToCartIcon();

        CartPage cartPage = new CartPage();
        cartPage.openCartPage();
        cartPage.incrementProductQuantity(0);
        cartPage.decrementProductQuantity(0);
        cartPage.decrementProductQuantity(0);

        boolean areAddedProductsInCart = page.getHeader().areAddedProductsInCart();
        assertFalse(areAddedProductsInCart, "Cart badge is not displayed when no products are added in cart.");
    }

    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider", description = "Logged user, from product details page add one metal mouse to cart, increment it once and remove it from cart page.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("When valid user is logged, from product details page add one metal mouse to cart, then go to cart page and increment product, then remove product from cart, the cart is empty now.")
    public void logged_user_add_to_cart_one_metal_mouse_then_increment_it_then_remove_product_from_cart(User user) {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();

        Product metalMagicMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMagicMouse.clickOnTheProductLink();
        ProductDetailsPage metalMouseDetailsPage = new ProductDetailsPage();
        metalMouseDetailsPage.clickOnTheAddToCartIcon();

        CartPage cartPage = new CartPage();
        cartPage.openCartPage();
        cartPage.incrementProductQuantity(0);
        cartPage.removeProductFromCart(0);

        boolean areThereProductsInCart = page.getHeader().areAddedProductsInCart();
        assertFalse(areThereProductsInCart, "Cart badge is not displayed when no products are added in cart.");
    }


    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider", description = "Logged user, from product details page add two different products to cart, and remove second product from cart page.")
    @Severity(SeverityLevel.CRITICAL)
    @Description("When valid user is logged, from product details page add one metal mouse to cart, then go to cart page and increment product, then remove product from cart, the cart is empty now.")
    public void logged_user_add_to_cart_two_different_products_remove_second_product_from_cart(User user) {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();

        Product metalMagicMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMagicMouse.clickOnTheProductLink();
        ProductDetailsPage metalMouseDetailsPage = new ProductDetailsPage();
        metalMouseDetailsPage.clickOnTheAddToCartIcon();
        page.openDemoShopApp();
        Product metalChair = new Product("3", "Awesome Metal Chair", "15.99");
        metalChair.clickOnTheProductLink();
        ProductDetailsPage chairDetailsPage = new ProductDetailsPage();
        chairDetailsPage.clickOnTheAddToCartIcon();
        page.openDemoShopApp();
        Product steelGloves = new Product("8","Licensed Steel Gloves","14.99");
        steelGloves.clickOnTheProductLink();
        ProductDetailsPage steelDetailsPage = new ProductDetailsPage();
        steelDetailsPage.clickOnTheAddToCartIcon();

        CartPage cartPage = new CartPage();
        cartPage.openCartPage();
        cartPage.incrementProductQuantity(0);
        cartPage.incrementProductQuantity(1);
        cartPage.incrementProductQuantity(2);

        cartPage.removeProductFromCart(1);


        page.openDemoShopApp();
        cartPage.openCartPage();
        sleep(3*1000);
//        cartPage.removeProductFromCart(0);

//        boolean areThereProductsInCart = page.getHeader().areAddedProductsInCart();
//        assertFalse(areThereProductsInCart, "Cart badge is not displayed when no products are added in cart.");
    }
}