**Demo Shop TAU AM**

Acest proiect este o demonstratie a automatizarii testelor pentru o aplicatie web de cumparaturi online. Scopul acestui proiect este de a arata modul de lucru cu Selenide si TestNG pentru a automatiza testele functionale pentru aplicatii web.

Acesta este proiectul final al lui Mihai Andreica in vederea promovarii cursului de Testare Software/ Quality Assurance cu acreditare.

**Autor**: Mihai Andreica

**Functionalitati**

Proiectul contine teste pentru urmatoarele functionalitati ale aplicatiei:
- Adaugare produs in cos si lista de dorinte
- Verificarea continutului cosului de cumparaturi si a listei de dorinte
- Autentificare utilizator
- Cautarea de produse
- Verificarea detaliilor produsului
- Finalizarea comenzii

**Clase**

Clasele sunt organizate in pachete, astfel incat fiecare clasa sa se ocupe de functionalitatile corespunzatoare. Pachetele sunt:
- **pages** - contine paginile aplicatiei web, cum ar fi pagina de autentificare, pagina de produse si pagina de finalizare a comenzii
- **data** - contine clase pentru gestionarea datelor de testare
- **tests** - contine teste pentru functionalitatile aplicatiei


Clasele specifice din proiect sunt:

- **CartPage** - clasa pentru pagina de cos de cumparaturi
- **CheckoutComplete** - clasa pentru pagina de finalizare a comenzii
- **CheckoutInfo** - clasa pentru informatiile despre plata si livrare
- **CheckoutSummary** - clasa pentru sumarul comenzii
- **DemoShopPage** - clasa pentru pagina principala a aplicatiei
- **Footer** - clasa pentru footer-ul aplicatiei
- **Header** - clasa pentru header-ul aplicatiei
- **LoginModal** - clasa pentru modalul de autentificare
- **Main** - clasa pentru continutul principal al paginii
- **Page** - clasa abstracta care defineste structura unei pagini web
- **Product** - clasa pentru detaliile unui produs
- **ProductDetailsPage** - clasa pentru pagina de detalii a produsului
- **ProductInCart** - clasa pentru produsul adaugat in cos
- **Wishlist** - clasa pentru lista de dorinte


**Teste**

In total, aplicația ruleaza 89 de teste care acopera diverse funcționalitati ale aplicației Demo Shop TAU AM.
Testele sunt organizate in pachete in functie de functionalitatea testata. Testele includ:

- **AddToCartAndWishlistTest** - testeaza adaugarea unui produs in cos si in lista de dorinte
- **AddToCartTest** - testeaza adaugarea unui produs in cos
- **CheckoutTest** - testeaza finalizarea comenzii
- **IncrementDecrementAndDeleteFromCartPageTest** - testeaza functionalitatea de incrementare/decrementare si stergere a produselor din cosul de cumparaturi
- **LoginTest** - testeaza autentificarea utilizatorilor
- **LogoutTest** - testeaza deconectarea utilizatorilor
- **ProductsCartVerification** -logat sau nelogat - testeaza verificarea continutului cosului de cumparaturi pentru diferite tipuri de utilizatori
- **SearchFieldTest** - testeaza functionalitatea de cautare a produselor site-ului dupa un cuvant cheie



**Tehnologii utilizate**:
- Java
- Selenium WebDriver
- TestNG
- Selenide
- Allure Framework
- Maven

**Configurare**:

Pentru a rula testele de automatizare, este necesara instalarea Java Development Kit (JDK) si Maven. 

Dupa instalare, se pot rula testele de automatizare cu comanda 'mvn clean test', si se pot obtine rapoartele cu ajutorul comenzilor 'mvn allure:report' & 'mvn allure:serve'.

**Git clone:** https://gitlab.com/MihaiAndreica/demo-shop-tau-am
